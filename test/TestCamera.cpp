#include "TestCamera.h"

#include<iostream>
#include "Helper.h"

bool TestCamera::TestMoveCamera(double camx, double camy, double pixelx, double pixely,
                    double linex1, double liney1, double linex2, double liney2, double groundTruth)
{
    return true;
}

bool TestCamera::TestRayInitialization(double camx, double camy, size_t pixels)
{
    auto cam = CameraScreen(Eigen::Vector2d(camx,camy),pixels);
    auto rays = cam.GetRays();
    for (auto &&ray : rays)
    {
        std::cout<<'('<<ray.x()<<", "<<ray.y()<<")\n";
    }
    return 0;
}