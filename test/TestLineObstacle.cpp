#include "TestLineObstacle.h"

#include<iostream>
#include "LineObstacle.h"
#include "Helper.h"

bool TestLineObstacle::TestGetIntercept(double camx, double camy, double pixelx, double pixely,
                    double linex1, double liney1, double linex2, double liney2, double groundTruth)
{
    auto cam = Eigen::Vector2d(camx,camy);
    auto pixel = Eigen::Vector2d(pixelx,pixely);
    auto lineStart = Eigen::Vector2d(linex1,liney1);
    auto lineEnd = Eigen::Vector2d(linex2,liney2);
    auto lineObst = LineObstacle(lineStart,lineEnd);
    auto result = lineObst.GetIntercept(pixel-cam,cam);
    if (not Helper::double_equals(result,groundTruth)) return 1;
    return 0;
}
