#ifndef TESTCAMERA_H
#define TESTCAMERA_H

#include<iostream>
#include<Eigen/Dense>

#include "CameraScreen.h"

namespace TestCamera
{
    bool TestMoveCamera(double camx, double camy, double stepx, double stepy,
        double linex1, double liney1, double linex2, double liney2, double groundTruth);

    bool TestRayInitialization(double camx, double camy, size_t pixels);
}

#endif