#ifndef TESTLINEOBSTACLE_H
#define TESTLINEOBSTACLE_H

#include<iostream>
#include<Eigen/Dense>

namespace TestLineObstacle
{
    bool TestGetIntercept(double camx, double camy, double pixelx, double pixely,
        double linex1, double liney1, double linex2, double liney2, double groundTruth);
}

#endif