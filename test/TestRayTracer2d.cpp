#include<cassert>
#include<iostream>
#include "TestLineObstacle.h"
#include "TestCamera.h"

int main(int argc, char const *argv[])
{
    if (argc == 1)
    {
        std::cout<<"This isn't how you run tests. Please either see the tasks.json file, \n\
                    or (better) the build/test/CTestTestfile.cmake file for exact commands \n\
                    for the tests.\n";
        exit(1);
    }
    int test = std::atoi(argv[1]);
    switch (test)
    {
        case 0: 
            return 0;
        case 1:
            return TestLineObstacle::TestGetIntercept(std::stod(argv[2]),std::stod(argv[3]),
                std::stod(argv[4]),std::stod(argv[5]),std::stod(argv[6]),std::stod(argv[7]),
                std::stod(argv[8]),std::stod(argv[9]),std::stod(argv[10]));
        case 2:
            return TestCamera::TestMoveCamera(std::stod(argv[2]),std::stod(argv[3]),
                std::stod(argv[4]),std::stod(argv[5]),std::stod(argv[6]),std::stod(argv[7]),
                std::stod(argv[8]),std::stod(argv[9]),std::stod(argv[10]));
        case 3:
            return TestCamera::TestRayInitialization(std::stod(argv[2]), std::stod(argv[3]),
                std::stoi(argv[4]));
        default:
            exit(-1); //no such test
    }
    /* code */
    return 0;
}
