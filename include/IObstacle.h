#ifndef IOBSTACLE_H
#define IOBSTACLE_H

#include<Eigen/Dense>
#include<exception>

/**
 * @brief Interface for obstacles to inherit
 * 
 */
class IObstacle
{
private:
public:
    /**
     * @brief Checks if ray intercepts obstacle. Return minimum distance if
     * intercept, return negative if no intercept
     * 
     * @return double; distance to obstacle; negative if no intercept
     */
    virtual double GetIntercept(Eigen::Vector2d ray_vec, Eigen::Vector2d ray_src)=0;// const { throw 20;};
    virtual ~IObstacle(){};
};

#endif