#if !defined(DISPLAY_H)
#define DISPLAY_H

#include <vector>

#include <SDL2/SDL.h>

class Display
{
private:
    const int _w;
    const int _h;
    const size_t _pixels;
    const int _band_w; // width of a band representign a single pixel, in pixels

    Uint32 * _colormap_pixel_vals;
    Uint32 * _display_pixel_vals;
    SDL_Window *_window;
    SDL_Renderer *_renderer;
    SDL_Texture * _colormap_texture;
    SDL_Texture * _display_texture;
    SDL_Rect _colormap;
    SDL_Rect _display;
public:
    /**
     * @brief Construct a new Display object
     * 
     * @param w width in pixels, default 800
     * @param h height in pixels default 200
     * @param pixels number of pixels on CameraScreen, default 10
     */
    Display(size_t pixels=100, int w = 800, int h=200);

    /**
     * @brief Update the display based on new pixel measurements
     * 
     * @param pixels a vector<double> with the distances measured by each pixel
     */
    void Update(const std::vector<double>& pixels) const;

    ~Display();
};

#endif // DISPLAY_H
