#ifndef LINEOBSTACLE_H
#define LINEOBSTACLE_H

#include "IObstacle.h"
#include<Eigen/Dense>
class LineObstacle : public IObstacle
{
private:
    Eigen::Vector2d _start;
    Eigen::Vector2d _vec;
public:
    /**
     * @brief Construct a new Line Obstacle object from the start and end points
     * 
     * @param start first point of the line
     * @param end end point of the line; global_end if isGlobalEnd, else global_end-global_start
     * @param globalEnd is end global or local to start; default=true
     */
    LineObstacle(Eigen::Vector2d start, Eigen::Vector2d end, bool isGlobalEnd = true);


    /**
     * @brief Construct a new Line Obstacle object from start and end coordinates
     * 
     * @param startx x coordinate of end point of the line; 
     * @param starty y coordinate of end point of the line; 
     * @param endx x coordinate of end point of the line; global_end if isGlobalEnd, else global_end-global_start
     * @param endy y coordinate of end point of the line; global_end if isGlobalEnd, else global_end-global_start
     * @param isGlobalEnd is end global or local to start; default=true
     */
    LineObstacle(double startx, double starty, double endx, double endy, bool isGlobalEnd = true);

    /**
     * @brief Get the distance from the camera to the LineObstacle along a given ray
     * 
     * @param ray_vec vector (direction) of the ray
     * @param ray_src camera location
     * @return double distance; negative if no intercept
     */
    double GetIntercept(Eigen::Vector2d ray_vec, Eigen::Vector2d ray_src) override;
    /**
     * @brief helper function for calculating interceptions between lines.
     * Returns vec1.x()*vec2.y() - vec1.y()*vec2.x()
     * @param vec1 
     * @param vec2 
     * @return double 
     */
    double Cross2d(const Eigen::Vector2d& vec1, const Eigen::Vector2d& vec2) const;
    LineObstacle()=default;
    ~LineObstacle()=default;
};

#endif