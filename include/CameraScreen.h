#ifndef CAMERASCREEN_H
#define CAMERASCREEN_H

#include<vector>
#include<Eigen/Dense>

enum Direction { Up, Down, Right, Left};

class CameraScreen
{
private:
    size_t _pixels;
    ushort _FoVDegrees;
    // orientation is from x axis in positive (anti-clockwise) direction
    ushort _orientationDegrees;
    Eigen::Vector2d _camPosition;
    std::vector<Eigen::Vector2d> _rays;

    /**
     * @brief internal function for rotating a point around another point in a
     * clockwise direction.
     * 
     * @param angle in degrees
     * @param point to be rotated
     * @param center of rotation
     * @return Eigen::Vector2d new location of rotated point
     */
    static Eigen::Vector2d RotatePointAnticlockwise(double angle, 
                    Eigen::Vector2d point, Eigen::Vector2d center = Eigen::Vector2d(0,0));

public:
   /**
    * @brief Create a CameraScreen object with an orientation and resolution
    * Initializes the _rays vector
    * 
    * @param camPosition camera's origin
    * @param pixels number of _pixels (number of _rays)
    * @param FoVDegrees field of view in degrees, default 90
    * @param orientationDegrees orientation of camera measured in degrees anticlockwise
    *       from the positive x axis
    */
    CameraScreen(Eigen::Vector2d camPosition, size_t pixels, ushort FoVDegrees = 120, 
                  ushort orientationDegrees = 0);

    /**
     * @brief Get the vector of _rays starting at the camera and passing
     * through each pixel, given as relative points (2d vector end with assumed 0,0 start)
     * 
     * @return std::vector<Eigen::Vector2d> 
     */
    const std::vector<Eigen::Vector2d>& GetRays() const;

    /**
     * @brief Get the Camera location
     * 
     * @return Eigen::Vector2d 
     */
    Eigen::Vector2d GetCamera() const;

    int GetPixels() const;

    /**
     * @brief Moves the camera one step in the required relative direction, everytime it is called
     * 
     * @param dir DIrection enum - Up Down Left Right
     * @param step length of movement step
     */
    void MoveCamera(Direction dir, double step = 1.0);

    /**
     * @brief rotates the camera anti-clockwise 
     * 
     * @param angle in degrees
     */
    void RotateCameraAntiClockwise(double angle);

    CameraScreen()=default;
    ~CameraScreen()=default;

 };

#endif