#ifndef HELPER_H
#define HELPER_H
#include<cstdlib>
class Helper
{
public:
    /**
     * @brief A rounding error-aware comparison for variables of type double
     * 
     * @param a first value to compate
     * @param b second value to compate
     * @param epsilon maximum difference to consider still equal
     * @return true 
     * @return false 
     */
    static bool double_equals(double a, double b, double epsilon = 0.00001)
    {
        return std::abs(a - b) < epsilon;
    }
};

#endif