#ifndef WORLD_H
#define WORLD_H

#include<vector>
#include <memory>

#include "IObstacle.h"
#include "CameraScreen.h"
#include "Display.h"

#define RESOLUTION 800

class World
{
private:
    int _maxfps = 60;
    int _min_frametime = 1000/_maxfps;
    bool _world_updated = true;
    std::vector<double> _distances;
    std::vector<Uint32> _frame_times;
    std::vector<std::unique_ptr<IObstacle>> _generalObstacles;
    CameraScreen _camera;
    Display _display;
    SDL_Event _event;

// #ifndef NDEBUG
    void WriteLog();
// #endif
    
public:
    /**
     * @brief Constructor, creates a default instance of the world with the cam
     * at the center and the screen 1 unit to the right. There are obstacles
     * around arranged in a maze-like pattern (see implementation for details) 
     * 
     */
    World();

    /**
     * @brief Checks whether the underlying datamembers have been changed
     * (through flag) and updates the display accordingly.
     * Handles user interaction events. 
     */
    void Update();

    /**
     * @brief Adds an obstacle to the world (move the unique pointer)
     * Sets world updated flag to true
     * 
     * @param objectStart 
     * @param objectEnd 
     */
    void AddObstacle(std::unique_ptr<IObstacle> obstacle);
   
    /**
     * @brief Gets distance to nearest object for each pixel
     * 
     * @param allRays 
     */
    void CalculateIntersections(const std::vector<Eigen::Vector2d>& allRays);

  
    /**
     * @brief Prints current measured distances 
     */
    void PrintOnScreen() const;

    /**
     * @brief Move camera in a direction by an increment.
     * Sets world updated flag to true
     * 
     * @param dir direction (enum -> Up Down Right Left)
     * @param step increment
     */
    void MoveCamera(Direction dir, double step = 1.0);

    /**
     * @brief Rotates camera (technically screen around camera which is a point)
     * Sets world updated flag to true
     * 
     * @param angle (in degrees)
     */
    void RotateCameraAntiClockwise(double angle);
};

#endif