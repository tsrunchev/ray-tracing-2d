#include "World.h"

#include <fstream>
#include <iostream>
#include <numeric>

#include "LineObstacle.h"

World::World() : //default world, to be extended for other configurations
    _camera(Eigen::Vector2d(0.0,0.0), //camera at center
            RESOLUTION),
    _distances(RESOLUTION,-1.0),
    _display(RESOLUTION,800,200)
{
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(1,0,1,1)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(1,1,-1,1)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-1,1,-1,-1)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-1,-1,1,-1)));

    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-2,-2,2,-2)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(2,-2,2,2)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(2,2,-2,2)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-2,2,-2,-1)));

    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(3,1,3,3)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(3,3,-3,3)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-3,3,-3,-3)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-3,-3,3,-3)));
    _generalObstacles.push_back(move(std::make_unique<LineObstacle>(3,-3,3,0)));
    // _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-1,1,2,1)));
    
    // _generalObstacles.push_back(move(std::make_unique<LineObstacle>(-1,-1,2,-1)));

    Update();
     /**
    These obstacles construct the following "maze"
     ____________
    |  ________  |
    | |  ____  | |
    | | |    | | 
    | | |____  | |
    |  ________| |
    |____________|
    **/
}

void World::AddObstacle(std::unique_ptr<IObstacle> obstacle)
{
    _generalObstacles.push_back(std::move(obstacle));
    _world_updated = true;
}

void World::CalculateIntersections(const std::vector<Eigen::Vector2d>& allRays) 
{
    int i=0;
    double newDistance{0.0};
    _distances = std::vector<double>(_camera.GetPixels(),-1.0);
    std::cout<<std::endl;
    for (auto &&ray : allRays)
    {
        for (auto &&obs : _generalObstacles)
        { 
            newDistance = obs->GetIntercept(ray, _camera.GetCamera());
            // negative values mean no intersection, or default value
            auto isOldValid = _distances[i] >= 0.0;
            auto isNewValid = newDistance >= 0.0;

            if (not isNewValid) continue;

            // then new result is valid 
            if ((not isOldValid) || newDistance<_distances[i])
                _distances[i] = newDistance;
            // std::cout<<_distances[i]<<' ';
        }
        i++;
    }
    // std::cout<<" I set"<<std::endl<<std::endl;
    _world_updated = false;
}

void World::PrintOnScreen() const
{
    std::cout<<std::endl<<"Distances: ";
    for (auto &&d : _distances)
    {
        std::cout<<d<<' ';
    }
    std::cout<<std::endl;
}

void World::MoveCamera(Direction dir, double step)
{
    _camera.MoveCamera(dir, step);
    _world_updated = true;
}

void World::RotateCameraAntiClockwise(double angle) 
{
    _camera.RotateCameraAntiClockwise(angle);
    _world_updated = true;
}

void World::Update()
{   
    auto frameStart = SDL_GetTicks();
    while(SDL_PollEvent(&_event))
    {
        switch(_event.type)
        {
            case SDL_QUIT:
            // #ifndef NDEBUG
                World::WriteLog();
            // #endif
                exit(0);
                break;
            case SDL_WINDOWEVENT_MOVED:
                _world_updated = true;
            case SDL_MOUSEMOTION://this doesn't work on WSL/Windows
                // std::cout<< "mouse moved " << event.motion.xrel<< std::endl;
                // int x,y;
                // SDL_GetRelativeMouseState(&x,&y);
                // std::cout<< "mouse moved other " << x<< std::endl;
                break;
            case SDL_KEYDOWN:
                const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
                if (currentKeyStates[SDL_SCANCODE_UP] || currentKeyStates[SDL_SCANCODE_W])
                {
                    MoveCamera(Up,.05);
                    std::cout<<"Up"<<std::endl;
                }
                if (currentKeyStates[SDL_SCANCODE_LEFT] || currentKeyStates[SDL_SCANCODE_A])
                {
                    RotateCameraAntiClockwise(5);
                    std::cout<<"turn left"<<std::endl;
                }
                if (currentKeyStates[SDL_SCANCODE_DOWN] || currentKeyStates[SDL_SCANCODE_S])
                {
                    MoveCamera(Down,.05);
                    std::cout<<"down"<<std::endl;
                }
                if (currentKeyStates[SDL_SCANCODE_RIGHT] || currentKeyStates[SDL_SCANCODE_D])
                {
                    RotateCameraAntiClockwise(-5);
                    std::cout<<"turn right"<<std::endl;
                } 
                if (currentKeyStates[SDL_SCANCODE_Q])
                {
                    MoveCamera(Left,.05);
                    std::cout<<"left";
                }
                if (currentKeyStates[SDL_SCANCODE_E])
                {
                    MoveCamera(Right,.05);
                    std::cout<<"right";
                }
                break;
        }
    }
    
    // if world has been updated somehow recalculate intersections
    if (_world_updated)
    {
        CalculateIntersections(_camera.GetRays());
        // update display
        _display.Update(_distances);
    }


    // Limit framerate
    auto frameEnd = SDL_GetTicks();
    auto frameTime = frameEnd - frameStart;
// #ifndef NDEBUG
    //PrintOnScreen();
    _frame_times.push_back(frameTime);
// #endif
    if (frameTime < _min_frametime)
        SDL_Delay(_min_frametime - frameTime);
}

void World::WriteLog()
{
    auto const count = static_cast<float>(_frame_times.size());
    auto const mean  = std::reduce(_frame_times.begin(), _frame_times.end()) / count;
    auto const maxFPS = 1000/mean;
    std::ofstream log_file(
        "log_file.txt", std::ios_base::out | std::ios_base::app );
    for (auto &&i : _frame_times)
    {
        log_file << i <<std::endl;
    }
    log_file << "MAX FPS: " << maxFPS;
}
