#include "Display.h"

#include <bits/stdc++.h>
#include <exception>
#include <string>
#include <iostream>

Display::Display(size_t pixels, int w, int h):
    _w(w),_h(h),_pixels(pixels),_band_w(_w*9/10/_pixels),
    _colormap{_w*19/20,0,_w/20,_h},
    _display{0,0,_w*19/20,_h}
{
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
        throw std::runtime_error("SDL could not initialize! SDL_Error: " + std::string(SDL_GetError()));

    _window = SDL_CreateWindow(
    "SDL2Test",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    _w,
    _h,
    SDL_WINDOW_INPUT_FOCUS
    );
    if( _window == NULL )
        throw std::runtime_error("Window could not be created! SDL_Error: " + std::string(SDL_GetError()));

    // this doesn't work on WSL/Windows
    // int rel_mouse = SDL_SetRelativeMouseMode(SDL_TRUE);
    // if(rel_mouse!=0)
    //     throw std::runtime_error("Relative mouse mode could not be set!");

    _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
    if(_renderer == NULL)
        throw std::runtime_error("Renderer could not be created! SDL_Error: " + std::string(SDL_GetError()));

    _colormap_texture = SDL_CreateTexture(_renderer,SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, 1, 255);
    _colormap_pixel_vals = new Uint32[255];

    _display_texture = SDL_CreateTexture(_renderer,SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, _pixels, 1);
    _display_pixel_vals = new Uint32[_pixels];

    // Set the colormap to the full range of red values; this won't be changed
    for (size_t i = 0; i < 255; i++)
    {
        auto format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);
        _colormap_pixel_vals[i] = SDL_MapRGBA(format, i/2, i, i,SDL_ALPHA_OPAQUE);
    }
    SDL_UpdateTexture(_colormap_texture, NULL, _colormap_pixel_vals, sizeof(Uint32));
    SDL_RenderCopy(_renderer, _colormap_texture, NULL, &_colormap);
}

void Display::Update(const std::vector<double>& pixels) const
{
    // dynamic range 
    // auto min = *std::min_element(pixels.begin(),pixels.end());
    // auto max = *std::max_element(pixels.begin(),pixels.end());
    // auto amp = max - min;

    // set display texture values (CameraScreen distance measurements)
    for (int i = 0; i < _pixels; i++)
    {
        // dynamic range
        // auto color = (pixels[i]-min)*255/amp;
        auto max_dist = 2.5;
        double distance = pixels[i];
        if (pixels[i] < 0 || pixels[i] > max_dist) distance = max_dist;

        _display_pixel_vals[i] = _colormap_pixel_vals[255-static_cast<int>(distance/max_dist*255)];//log2(distance/max_dist)*255;
    }
    SDL_UpdateTexture(_display_texture, NULL, _display_pixel_vals, sizeof(Uint32));
    SDL_RenderCopy(_renderer,_display_texture, NULL, &_display);
    // std::cout<<" I show"<<std::endl<<std::endl;

    // I only see the colormap every second frame otherwise
    SDL_RenderCopy(_renderer, _colormap_texture, NULL, &_colormap);
    SDL_SetRenderDrawColor(_renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawRect(_renderer, &_colormap);

    // SDL_RenderClear(renderer);
    SDL_RenderPresent(_renderer);
}

Display::~Display()
{
    delete[] _colormap_pixel_vals;
    SDL_DestroyTexture(_colormap_texture);
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
    SDL_Quit();
}