#include <iostream>
#include <vector>
#include <memory>
#include <thread>         
#include <chrono>
#include "CameraScreen.h"
#include "IObstacle.h"
#include "LineObstacle.h"
#include "World.h"
/**
 * @brief  Defines a CameraScreen object, adds obstacles, calls loop
 * 
 * @return int 0 if success, negative error code otherwise
 */

int main(int, char**) {
    
    World world;
    while (true)
    {
        world.Update();
    }
    
    return 0;
}
