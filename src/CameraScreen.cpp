#include<iostream>
#include "CameraScreen.h"
#include "IObstacle.h"

CameraScreen::CameraScreen(Eigen::Vector2d camPosition, size_t pixels, ushort FoVDegrees, 
                        ushort orientationDegrees): _camPosition(camPosition), _pixels(pixels),
                                    _FoVDegrees(FoVDegrees), _orientationDegrees(orientationDegrees),
                                     _rays(pixels)
{
     for(int i=0; i<_pixels; i++){
        _rays[i] = RotatePointAnticlockwise(_FoVDegrees/2 - 1.0*_FoVDegrees/(_pixels-1)*i, Eigen::Vector2d(1.0,0.0));
    }
}

Eigen::Vector2d CameraScreen::GetCamera() const
{
     return _camPosition;
}

int CameraScreen::GetPixels() const
{
     return _pixels;
}

const std::vector<Eigen::Vector2d>& CameraScreen::GetRays() const
{
    return _rays;
}

void CameraScreen::MoveCamera(Direction dir, double step)
{
    // default move forward, default forward is along x axis
    auto movementOrientation = _orientationDegrees;
    auto dir_vec = Eigen::Vector2d(step,0.0);
    switch (dir) {
        case Up:     // move up
            {
                // already good
                break;
            }
        case Down:     // move down
            {
                movementOrientation += 180;
            break;
            }
        case Right:     // move right
            {
                movementOrientation -= 90;
            break;
            }
        case Left:     // move left
            {
                movementOrientation += 90; 
            break;
            }
    }
    if ( movementOrientation>360 )
        movementOrientation -= 360;

    // rotate relative moevement vector around 0 to get correct movement direction
    _camPosition = _camPosition + RotatePointAnticlockwise(movementOrientation,dir_vec);
    #ifndef NDEBUG
    std::cout<<"Cam coords (x,y): ("<<_camPosition.x()<<", "<< _camPosition.y()<<")\n";
    #endif
}


Eigen::Vector2d CameraScreen::RotatePointAnticlockwise(double angle, Eigen::Vector2d point, Eigen::Vector2d center)
{
    // source: https://stackoverflow.com/questions/2259476/rotating-a-point-about-another-point-2d
    auto rad = 0.017453;
    angle = angle*rad;
    auto rel_point = point - center;
    auto p_x = cos(angle)*rel_point.x() - sin(angle)*rel_point.y() + center.x();
    auto p_y = sin(angle)*rel_point.x() + cos(angle)*rel_point.y() + center.y();
    return Eigen::Vector2d(round(p_x*1000),round(p_y*1000))/1000.0;
}

void CameraScreen::RotateCameraAntiClockwise(double angle)
{   
    _orientationDegrees += angle;
    if (_orientationDegrees >= 360)
        _orientationDegrees -= 360;
    for(int i=0; i<_pixels; i++){
        _rays[i] = RotatePointAnticlockwise(angle,_rays[i]);
    }
}
