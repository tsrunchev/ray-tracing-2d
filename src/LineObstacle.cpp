#include<cmath>
#include "LineObstacle.h"
#include "Helper.h"

LineObstacle::LineObstacle(Eigen::Vector2d start, Eigen::Vector2d end, bool isGlobalEnd):
_start(start)
{
    if (isGlobalEnd) _vec = end - _start;
    else _vec = end;
}

LineObstacle::LineObstacle(double startx, double starty, double endx, double endy, bool isGlobalEnd):
_start(startx,starty)
{   
    auto end = Eigen::Vector2d(endx,endy);
    if (isGlobalEnd) _vec = end - _start;
    else _vec = end;
}

double LineObstacle::GetIntercept(Eigen::Vector2d ray_vec, Eigen::Vector2d ray_src) 
{
    // see https://stackoverflow.com/a/565282 for naming and logic

    auto p = ray_src; //camera
    auto r = ray_vec;
    auto s = _vec;
    auto q = _start;

    auto parallel = Cross2d(r,s);
    if (parallel==0)
    {
        auto colinear = Cross2d(q-p,r);
        if (colinear != 0) return -2.0; // no intersection
        auto t0 = (q-p).dot(r)/r.norm();
        auto t1 = t0+s.dot(r)/r.norm();
        auto ist0lt0 = t0 < 0;
        auto ist1lt0 = t1 < 0;
        if (ist0lt0 && ist1lt0) return -2.0; // line behind cam
        if (ist0lt0 || ist1lt0) return 0.0; //camera sitting on line
        return std::min(t0,t1); // both ends at or in front of cam, return nearer
    }
    auto t = Cross2d((q-p),s)/parallel;
    if (t<0) return -1.0; // object behind camera
    auto u = Cross2d((q-p), r)/parallel;
    if ((0.0 <= u) && (u <= 1.0)) return t*r.norm();
    else return -1.0;
}

double LineObstacle::Cross2d(const Eigen::Vector2d& vec1, const Eigen::Vector2d& vec2) const
{
    return vec1.x()*vec2.y() - vec1.y()*vec2.x();
}